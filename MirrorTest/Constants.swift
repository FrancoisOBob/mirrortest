//
//  Const.swift
//  MirrorTest
//
//  Created by François Lambert on 23/07/2018.
//  Copyright © 2018 François Lambert. All rights reserved.
//

import Foundation

let BuildIdentifier = "com.test.MirrorTest"

struct MirrorURLs {
    static let Auth = "https://mirror-ios-test.herokuapp.com/auth"
    static let Users = "https://mirror-ios-test.herokuapp.com/users"
}

enum Localizable {
    enum Login: String {
        case Title = "Log in"
        case UsernamePlaceholder = "Username..."
        case PasswordPlaceholder = "Password..."
        case ButtonAccessTitle = "LOG IN"
        case ButtonModeTitle = "Not yet a member ? Register"
        case ErrorMessage = "Invalid credentials"
    }
    
    enum Register: String {
        case Title = "Account creation"
        case UsernamePlaceholder = "Username..."
        case PasswordPlaceholder = "Password..."
        case ButtonAccessTitle = "CREATE AN ACCOUNT"
        case ButtonModeTitle = "Already a member ? Log in"
        case ErrorMessage = "This username already exists"
    }
    
    enum Account: String {
        case Title = "My Account"
        case LogoutButton = "Logout"
        case Username = "Username"
        case Age = "Age (y.)"
        case Height = "Height (cm)"
        case LikesJS = "Likes JS"
        case MagicNumber = "Magic Number"
        case MagicHash = "Magic Hash"
    }
}

enum StoryboardID: String {
    case Login = "Login"
    case Account = "Account"
}

enum KeychainKey: String {
    case Username = "kUsername"
    case Password = "kPassword"
    case Token = "kToken"
}

enum APIKey: String {
    case Username = "username"
    case Password = "password"
    case Id = "id"
    case Age = "age"
    case Height = "height"
    case LikesJS = "likes_javascript"
    case MagicNumber = "magic_number"
    case MagicHash = "magic_hash"
    case AccessToken = "access_token"
}
