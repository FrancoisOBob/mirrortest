//
//  RootInteractor.swift
//  MirrorTest
//
//  Created by François Lambert on 23/07/2018.
//  Copyright © 2018 François Lambert. All rights reserved.
//

class RootInteractor: Interactor {

    /// The presenter var will store a reference to the RootPresenter
    var presenter: RootPresenter
    
    init(presenter: RootPresenter) {
        self.presenter = presenter
    }
    
    func tryLogin() {
        APIHandler.shared.autoLogin { [weak self] success in
            self?.presenter.show(success ? .Account : .Login)
        }
    }
}
