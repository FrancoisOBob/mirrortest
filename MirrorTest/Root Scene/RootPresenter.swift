//
//  RootPresenter.swift
//  MirrorTest
//
//  Created by François Lambert on 23/07/2018.
//  Copyright © 2018 François Lambert. All rights reserved.
//

class RootPresenter: Presenter {

    weak var viewController: RootViewController?
    
    /**
     We setup the RootViewController in the
     RootPresenter, because only the Presenter
     can update the viewController
     */
    init(vc: RootViewController) {
        viewController = vc
    }
    
    func show(_ identifier: StoryboardID) {
        viewController?.show(identifier)
    }
}
