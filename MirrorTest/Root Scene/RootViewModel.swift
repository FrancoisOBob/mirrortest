//
//  RootViewModel.swift
//  MirrorTest
//
//  Created by François Lambert on 23/07/2018.
//  Copyright © 2018 François Lambert. All rights reserved.
//

/// struct RootViewModel
///
/// Will be used to transit data to the presenter and the ViewController
/// >>> No need for a model here, nothing to display in the ViewController
///
struct RootViewModel {}
