//
//  RootViewController.swift
//  MirrorTest
//
//  Created by François Lambert on 23/07/2018.
//  Copyright © 2018 François Lambert. All rights reserved.
//

import UIKit

class RootViewController: UIViewController, VIPViewController {
    var interactor: RootInteractor!
    
    var model: RootViewModel?
    
    // MARK: Object lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        interactor = RootInteractor(presenter: RootPresenter(vc: self))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        interactor.tryLogin()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /**
     layout() is called by the presenter AFTER being requested by the interactor
     AKA: Display the result from the Presenter
     >> nothing to display here
     */
    func layout(_ model: RootViewModel) {}
    
    func show(_ identifier: StoryboardID) {
        // switch root view controller to Access Scene
        guard let window = UIApplication.shared.keyWindow else {
            return
        }
        window.rootViewController = self.storyboard?.instantiateViewController(withIdentifier: identifier.rawValue)
    }
}


