//
//  AccountInteractor.swift
//  MirrorTest
//
//  Created by François Lambert on 24/07/2018.
//  Copyright © 2018 François Lambert. All rights reserved.
//

import UIKit

class AccountInteractor: Interactor {

    var presenter: AccountPresenter
    
    init(presenter: AccountPresenter) {
        self.presenter = presenter
    }
    
    func fetch() {
        presenter.present()
        
        // TODO : no user in session after a simple login -> no way to get the id from auth with username/password
        APIHandler.shared.fetch() { [weak self] (user) in
            self?.display(user)
        }
    }
    
    func update(_ key: String, value: Any) {
        APIHandler.shared.update(key, value: value) { [weak self] (user) in
            // self?.display(user)
            print("User updated : \(user)")
        }
    }
    
    /// Make unit testing easier
    func display(_ user: User?) {
        guard let user = user else { return }
        presenter.present(user)
    }
}
