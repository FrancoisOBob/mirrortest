//
//  AccountSwitchTableViewCell.swift
//  MirrorTest
//
//  Created by François Lambert on 24/07/2018.
//  Copyright © 2018 François Lambert. All rights reserved.
//

import UIKit

class AccountSwitchTableViewCell: UITableViewCell, TableViewCellProtocol {

    var model: AccountSwitchRow?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var swiitch: UISwitch!
    
    func configure(viewModel model: AccountSwitchRow) {
        self.model = model
        
        titleLabel.text = model.title
        
        swiitch.isOn = model.isOn == true
    }

    @IBAction func didTapSwitch(_ sender: UISwitch) {
        model?.callback?(sender.isOn)
    }
}
