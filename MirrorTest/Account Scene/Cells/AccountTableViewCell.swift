//
//  AccountTableViewCell.swift
//  MirrorTest
//
//  Created by François Lambert on 24/07/2018.
//  Copyright © 2018 François Lambert. All rights reserved.
//

import UIKit

class AccountTableViewCell: UITableViewCell, TableViewCellProtocol {

    var model: AccountRow?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueTextField: UITextField!
    
    func configure(viewModel model: AccountRow) {
        self.model = model
        
        titleLabel.text = model.title
        
        valueTextField.placeholder = model.title
        valueTextField.text = model.value ?? ""
        valueTextField.isUserInteractionEnabled = model.editable ?? false
        valueTextField.keyboardType = model.keyboard ?? .default
    }
}

extension AccountTableViewCell: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        model?.callback?(textField.text ?? "")
    }
}
