//
//  AccountViewController.swift
//  MirrorTest
//
//  Created by François Lambert on 24/07/2018.
//  Copyright © 2018 François Lambert. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class AccountViewController: UITableViewController, VIPViewController {
    
    var interactor: AccountInteractor!
    
    var model: AccountViewModel?
 
    // MARK: Object lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        interactor = AccountInteractor(presenter: AccountPresenter(vc: self))
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        interactor.fetch()
        
        // IQKeyboardManager activation
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = false
    }
    
    // MARK: Display logic
    
    func display(_ model: AccountUIModel) {
        navigationItem.title = model.title
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: model.logoutButton,
                                                            style: .plain,
                                                            target: self,
                                                            action: #selector(logout))
        
    }
    
    /**
     layout() is called by the presenter AFTER being requested by the interactor
     AKA: Display the result from the Presenter
     >> nothing to display here
     */
    func layout(_ model: AccountViewModel) {
        self.model = model
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 70.0
        tableView.reloadData()
        
        refreshControl?.endRefreshing()
    }
    
    // MARK: User Actions
    
    @IBAction func refresh(_ sender: UIRefreshControl) {
        interactor.fetch()
    }
    
    @objc func logout() {
        APIHandler.shared.logout()
        
        show(.Login)
    }
    
    func show(_ identifier: StoryboardID) {
        // switch root view controller to login
        guard let window = UIApplication.shared.keyWindow else {
            return
        }
        window.rootViewController = self.storyboard?.instantiateViewController(withIdentifier: identifier.rawValue)
    }
}

extension AccountViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return model?.numberOfSections ?? 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model?.numberOfRows(forSection: section) ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let row = model?.rowViewModel(forIndexPath: indexPath) else {
            fatalError("Failed to retrieve row at index path \(indexPath)")
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: row.identifier, for:indexPath)
        row.configure(cell: cell)
        return cell
    }
}
