//
//  AccountViewModel.swift
//  MirrorTest
//
//  Created by François Lambert on 24/07/2018.
//  Copyright © 2018 François Lambert. All rights reserved.
//

import UIKit

struct AccountUIModel {
    let title: String
    let logoutButton: String
}

struct AccountViewModel: TableViewModel {
    var sections: Array<SectionViewModel>
}

struct AccountSection: SectionViewModel {
    var rows: Array<RowViewModel>
}

struct AccountRow: RowViewModel {
    let identifier = AccountTableViewCell.identifier
    
    let title: String?
    let value: String?
    let editable: Bool?
    let keyboard: UIKeyboardType?
    let callback: ((String) -> Void)?
    
    func configure(cell: UITableViewCell) {
        guard let cell = cell as? AccountTableViewCell else {
            fatalError("Failed to retrieve cell of type AccountTableViewCell")
        }
        cell.configure(viewModel: self)
    }
}

struct AccountSwitchRow: RowViewModel {
    let identifier = AccountSwitchTableViewCell.identifier
    
    let title: String?
    let isOn: Bool?
    let callback: ((Bool) -> Void)?
    
    func configure(cell: UITableViewCell) {
        guard let cell = cell as? AccountSwitchTableViewCell else {
            fatalError("Failed to retrieve cell of type AccountSwitchTableViewCell")
        }
        cell.configure(viewModel: self)
    }
}
