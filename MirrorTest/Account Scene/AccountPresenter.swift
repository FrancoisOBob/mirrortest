//
//  AccountPresenter.swift
//  MirrorTest
//
//  Created by François Lambert on 24/07/2018.
//  Copyright © 2018 François Lambert. All rights reserved.
//

import UIKit

class AccountPresenter: Presenter {
    
    weak var viewController: AccountViewController?
    
    /**
     We setup the AccountViewController in the
     AccountPresenter, because only the Presenter
     can update the viewController
     */
    init(vc: AccountViewController) {
        viewController = vc
    }
    
    func present() {
        viewController?.display(AccountUIModel(title: Localizable.Account.Title.rawValue,
                                              logoutButton: Localizable.Account.LogoutButton.rawValue))
    }
    
    func present(_ user: User) {
        
        var rows = [RowViewModel]()
        
        // Username
        rows.append(AccountRow(title: Localizable.Account.Username.rawValue,
                               value: user.username,
                               editable: false,
                               keyboard: .default,
                               callback: nil))
        
        // Age
        rows.append(AccountRow(title: Localizable.Account.Age.rawValue,
                               value: convert(ageInSeconds: user.age),
                               editable: true,
                               keyboard: .numberPad,
                               callback: { [weak self] value in
                                self?.viewController?.interactor.update(APIKey.Age.rawValue,
                                                                        value: convert(ageInYears: value) as Any)
        }))
        
        // Height
        rows.append(AccountRow(title: Localizable.Account.Height.rawValue,
                               value: user.height == nil ? nil : "\(String(describing: user.height!))",
                               editable: true,
                               keyboard: .numberPad,
                               callback: { [weak self] value in
            // TODO : Convertion from String to  Int
            self?.viewController?.interactor.update(APIKey.Height.rawValue, value: value)
        }))
        
        // Likes Javascript
        rows.append(AccountSwitchRow(title: Localizable.Account.LikesJS.rawValue,
                                     isOn: user.likesJavascript == true,
                                     callback: { [weak self] (isOn) in
            self?.viewController?.interactor.update(APIKey.LikesJS.rawValue, value: isOn)
        }))
        
        // Magic Number
        rows.append(AccountRow(title: Localizable.Account.MagicNumber.rawValue,
                               value: user.magicNumber == nil ? nil : "\(String(describing: user.magicNumber!))",
                               editable: true,
                               keyboard: .numberPad,
                               callback: { [weak self] value in
            self?.viewController?.interactor.update(APIKey.MagicNumber.rawValue, value: value)
        }))
        
        // Magic Hash
        rows.append(AccountRow(title: Localizable.Account.MagicHash.rawValue,
                               value: user.magicHash,
                               editable: true,
                               keyboard: .default,
                               callback: { [weak self] value in
            self?.viewController?.interactor.update(APIKey.MagicHash.rawValue, value: value)
        }))
        
        // Layout View Model
        viewController?.layout(AccountViewModel(sections: [AccountSection(rows: rows)]))
    }
    
}
