//
//  CleanSwift.swift
//  MirrorTest
//
//  Created by François Lambert on 23/07/2018.
//  Copyright © 2018 François Lambert. All rights reserved.
//

import UIKit

/// Protocol Interactor
protocol Interactor {
    associatedtype Presenter
    var presenter: Presenter { get set }
}

/// Protocol Presenter
protocol Presenter {
    associatedtype VVC : UIViewController
    var viewController: VVC? { get set }
}

/// extension Presenter
///
/// Will extend the presenter protocol to add functionnality
///
extension Presenter where VVC: VIPViewController {
    typealias ViewModel = VVC.ViewModel
    func present(_ viewModel: ViewModel) {
        viewController?.layout(viewModel)
    }
}

/// Protocol VIPViewController
///
/// Need to be implemented by all scenes
///
protocol VIPViewController {
    associatedtype Interactor
    associatedtype ViewModel
    var interactor: Interactor { get set }
    func layout(_ model: ViewModel)
}

/// extension VIPViewController
///
/// Connect interactor, presenter and ViewModel
///
extension VIPViewController { }
