//
//  TableViewModel.swift
//  MirrorTest
//
//  Created by François Lambert on 24/07/2018.
//  Copyright © 2018 François Lambert. All rights reserved.
//

import UIKit

protocol TableViewModel {
    /// Hold the sections of the view model
    var sections: Array<SectionViewModel> { get set }
    /// Return the number of sections of the view model
    var numberOfSections: Int { get }
    
    /**
     Configure a cell for a specific IndexPath
     - Parameters :
     - cell : the cell to configure
     - forIndexPath : the section and row to fetch the right view model
     */
    func configure(cell: UITableViewCell, forIndexPath index: IndexPath)
    
    /**
     The number of rows for a specific section
     - Parameters
     - section : the section
     - Returns
     - the number of rows for a specific section
     */
    func numberOfRows(forSection section: Int) -> Int
    
    /**
     Get the right view model for a specific section & row
     - Parameters
     - section: the section
     - row: the row of the section
     - Returns
     - the corresponding view model
     */
    func rowViewModel(forIndexPath index: IndexPath) -> RowViewModel?
}

/// Nice abstraction that represents a UITableView View Model
extension TableViewModel {
    var numberOfSections : Int { return sections.count }
    
    func numberOfRows(forSection section: Int) -> Int { return sections[section].numberOfRows }
    
    func configure(cell: UITableViewCell, forIndexPath index: IndexPath) {
        sections[index.section].configure(cell: cell, row: index.row)
    }
    
    func rowViewModel(forIndexPath index: IndexPath) -> RowViewModel? {
        guard index.section < sections.count else { return nil }
        let section = sections[index.section]
        guard index.row < section.rows.count else { return nil }
        return section.rows[index.row]
    }
}

// MARK: Section View Model

/// Nice interface that represents a UITableView Section View Model
protocol SectionViewModel {
    /// Hold the rows of the view model
    var rows : Array<RowViewModel> { get set }
    /// Return the number of rows of the view model
    var numberOfRows: Int { get }
    
    /**
     Configure a cell for a specific IndexPath
     - Parameters :
     - cell : the cell to configure
     - row : the row to fetch the right view model
     */
    func configure(cell: UITableViewCell, row: Int)
    
    /**
     Return the reuse identifier of the cell to use
     - Parameters :
     - row : the row to fetch the right identifier
     */
    func identifier(forRow row: Int) -> String
}

/// Nice abstraction that represents a UITableView Section View Model
extension SectionViewModel {
    var numberOfRows: Int { return rows.count }
    
    func configure(cell: UITableViewCell, row: Int) { rows[row].configure(cell: cell) }
    
    func identifier(forRow row: Int) -> String { return rows[row].identifier }
}

// MARK: Row View Model

protocol RowViewModel {
    var identifier: String { get }
    func configure(cell: UITableViewCell)
}

protocol TableViewCellProtocol {
    /// Any Model type adapting VCRowViewModel
    associatedtype ViewModel : RowViewModel
    
    func configure(viewModel model: ViewModel)
}

extension UITableViewCell {
    /// Default Identifier for reuse
    class var identifier: String {
        return String(describing: self)
    }
}
