//
//  Converter.swift
//  MirrorTest
//
//  Created by François Lambert on 24/07/2018.
//  Copyright © 2018 François Lambert. All rights reserved.
//

import Foundation

let secondsPerYear = 31557600

/// Convertion from age in seconds in Int to years in String
func convert(ageInSeconds: Int?) -> String? {
    guard let seconds = ageInSeconds, seconds > 0 else { return nil }
    let years = Double(seconds) / Double(secondsPerYear)
    return String(format: "%.0f", years)
}

/// Convertion from age in years in String to age in seconds in Int
func convert(ageInYears: String?) -> Int? {
    guard let years = ageInYears, let intYears = Int(years) else { return nil }
    return intYears * secondsPerYear
}

