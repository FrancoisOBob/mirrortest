//
//  AccessPresenter.swift
//  MirrorTest
//
//  Created by François Lambert on 23/07/2018.
//  Copyright © 2018 François Lambert. All rights reserved.
//

class AccessPresenter: Presenter {
    
    weak var viewController: AccessViewController?
    
    /**
     We setup the AccessViewController in the
     AccessPresenter, because only the Presenter
     can update the viewController
     */
    init(vc: AccessViewController) {
        viewController = vc
    }
    
    func present(_ mode: AccessMode) {
        
        switch mode {
        case .login:
            viewController?.layout(AccessViewModel(title: Localizable.Login.Title.rawValue,
                                                   usernamePlaceholder: Localizable.Login.UsernamePlaceholder.rawValue,
                                                   passwordPlaceholder: Localizable.Login.PasswordPlaceholder.rawValue,
                                                   buttonAccessTitle: Localizable.Login.ButtonAccessTitle.rawValue,
                                                   buttonModeTitle: Localizable.Login.ButtonModeTitle.rawValue))
        case .register:
            viewController?.layout(AccessViewModel(title: Localizable.Register.Title.rawValue,
                                                   usernamePlaceholder: Localizable.Register.UsernamePlaceholder.rawValue,
                                                   passwordPlaceholder: Localizable.Register.PasswordPlaceholder.rawValue,
                                                   buttonAccessTitle: Localizable.Register.ButtonAccessTitle.rawValue,
                                                   buttonModeTitle: Localizable.Register.ButtonModeTitle.rawValue))
        }
        
    }
    
    func presentLoading(_ on: Bool) {
        viewController?.displayLoading(on)
    }
    
    func show(_ identifier: StoryboardID) {
        viewController?.show(identifier)
    }
    
    func presentError(_ message: String) {
        viewController?.displayError(message)
    }
}
