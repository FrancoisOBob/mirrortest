//
//  LoginViewController.swift
//  MirrorTest
//
//  Created by François Lambert on 23/07/2018.
//  Copyright © 2018 François Lambert. All rights reserved.
//

import UIKit
import BRYXBanner
import IQKeyboardManagerSwift

class AccessViewController: UIViewController, VIPViewController {
    var interactor: AccessInteractor!
    
    var model: AccessViewModel?
    
    @IBOutlet var formElements: [UIView]!
    @IBOutlet weak var textFieldUsername: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var buttonAccess: UIButton!
    @IBOutlet weak var buttonMode: UIButton!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    
    // MARK: Object lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        interactor = AccessInteractor(presenter: AccessPresenter(vc: self))
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        interactor.load()
        
        // IQKeyboardManager activation
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = false
    }
    
    // MARK: Display logic
    
    /**
     layout() is called by the presenter AFTER being requested by the interactor
     AKA: Display the result from the Presenter
     >> nothing to display here
     */
    func layout(_ model: AccessViewModel) {
        navigationItem.title = model.title
        textFieldUsername.placeholder = model.usernamePlaceholder
        textFieldPassword.placeholder = model.passwordPlaceholder
        buttonAccess.setTitle(model.buttonAccessTitle, for: .normal)
        buttonMode.setTitle(model.buttonModeTitle, for: .normal)
    }
    
    func displayLoading(_ on: Bool) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = on
        loader.isHidden = !on
        formElements.forEach({
            $0.alpha = on ? 0.3 : 1
            $0.isUserInteractionEnabled = !on
        })
    }
    
    func displayError(_ message: String) {
        DispatchQueue.main.async {
            let banner = Banner(title: message,
                                backgroundColor: UIColor.red)
            banner.springiness = .heavy
            banner.show(duration: 2.5)
        }
    }
    
    // MARK: User actions
    
    @IBAction func didTapAccess(_ sender: UIButton) {
        print("Access button tapped !")
        interactor.access(username: textFieldUsername.text, password: textFieldPassword.text)
    }
    
    @IBAction func didTapSwitchMode(_ sender: UIButton) {
        interactor.switchMode()
    }
    
    func show(_ identifier: StoryboardID) {
        // switch root view controller to login
        guard let window = UIApplication.shared.keyWindow else {
            return
        }
        window.rootViewController = self.storyboard?.instantiateViewController(withIdentifier: identifier.rawValue)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension AccessViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == textFieldUsername {
            textFieldPassword.becomeFirstResponder()
        } else {
            interactor.access(username: textFieldUsername.text, password: textFieldPassword.text)
        }
        
        return false
    }
}
