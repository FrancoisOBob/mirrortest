//
//  AccessInteractor.swift
//  MirrorTest
//
//  Created by François Lambert on 23/07/2018.
//  Copyright © 2018 François Lambert. All rights reserved.
//

enum AccessMode {
    case login
    case register
}

class AccessInteractor: Interactor {
    
    /// The presenter var will store a reference to the AccessPresenter
    var presenter: AccessPresenter
    
    var mode: AccessMode = .register
    
    init(presenter: AccessPresenter) {
        self.presenter = presenter
    }
    
    func load() {
        presenter.present(mode)
    }
    
    func switchMode() {
        mode = (mode == .login) ? .register : .login
        load()
    }
    
    func access(username: String?, password: String?) {
        guard let username = username, !username.isEmpty,
            let password = password, !password.isEmpty else {
            return
        }
        presenter.presentLoading(true)

        APIHandler.shared.access(mode: mode, username: username, password: password) { [weak self] success in
            if success {
                self?.presenter.show(.Account)
            } else {
                self?.presenter.presentError(self?.mode == .login ? Localizable.Login.ErrorMessage.rawValue : Localizable.Register.ErrorMessage.rawValue)
                self?.presenter.presentLoading(false)
            }
        }
    }
}
