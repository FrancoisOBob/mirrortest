//
//  APIHandler.swift
//  MirrorTest
//
//  Created by François Lambert on 23/07/2018.
//  Copyright © 2018 François Lambert. All rights reserved.
//

import UIKit
import Alamofire
import KeychainAccess

class APIHandler: NSObject {
    
    static var shared = APIHandler()
    
    private let keychain = Keychain(service: BuildIdentifier)
    
    public var user: User?
    
    func autoLogin(completion: @escaping (Bool) -> Void) {
        // Username & Password in Keychain ?
        do {
            // Token can expire so we perform an authentication with username & password on application launch
            guard let username = try keychain.get(KeychainKey.Username.rawValue),
                let password = try keychain.get(KeychainKey.Password.rawValue) else {
                completion(false)
                return
            }
            
            // Username & Password retrieved ✔
            access(mode: .login, username: username, password: password) { (success) in
                completion(success)
            }
        } catch let error {
            print(error.localizedDescription)
            completion(false)
        }
    }
    
    func logout() {
        do {
            try keychain.remove(KeychainKey.Username.rawValue)
            try keychain.remove(KeychainKey.Password.rawValue)
            try keychain.remove(KeychainKey.Token.rawValue)
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    func access(mode: AccessMode, username: String, password: String, completion: @escaping (Bool) -> Void) {
        // Proper URL
        guard let url = URL(string: mode == .login ? MirrorURLs.Auth : MirrorURLs.Users) else {
            completion(false)
            return
        }
        
        // Perform API call
        Alamofire.request(url,
                          method: .post,
                          parameters: [APIKey.Username.rawValue: username, APIKey.Password.rawValue: password],
                          encoding: JSONEncoding.default)
            .validate()
            .responseJSON { (response) in
                
                guard response.result.isSuccess else {
                    print("Error while trying to access: \(String(describing: response.result.error))")
                    completion(false)
                    return
                }
                
                guard let value = response.result.value as? [String: Any] else {
                    print("Malformed data received from auth API")
                    completion(false)
                    return
                }
                
                switch mode {
                case .login:
                    guard let token = value[APIKey.AccessToken.rawValue] as? String else {
                        completion(false)
                        return
                    }
                    // Save Username, Password & Token in Keychain
                    self.keychain[KeychainKey.Username.rawValue] = username
                    self.keychain[KeychainKey.Password.rawValue] = password
                    self.keychain[KeychainKey.Token.rawValue] = token
                    
                    completion(true)
                    
                case .register:
                    self.user = User(json: value)
                    
                    print(value)
                    
                    // POST USERS do not returns access_token ... so let's go for a log in
                    
                    self.access(mode: .login, username: username, password: password, completion: { (success) in
                        completion(success)
                    })
                }
        }
    }
    
    func update(_ key: String, value: Any, completion: @escaping (User?) -> Void) {
        var parameters = [String: Any]()
        if let intValue = value as? Int {
            parameters = [key: intValue as Any]
        } else if let stringValue = value as? String {
            parameters = [key: stringValue as Any]
        } else if let boolValue = value as? Bool {
            parameters = [key: boolValue as Any]
        }
        users(parameters: parameters) { (user) in
            completion(user)
        }
    }
    
    func fetch(completion: @escaping (User?) -> Void) {
        users() { (user) in
            completion(user)
        }
    }
    
    private func users(parameters: [String: Any]? = nil, completion: @escaping (User?) -> Void) {
        
        do {
            guard let userID = user?.id,
                let url = URL(string: MirrorURLs.Users + "/\(userID)"),
                let token = try keychain.get(KeychainKey.Token.rawValue) else {
                    completion(self.user)
                    return
            }
            
            let headers: HTTPHeaders = ["Content-Type": "application/json",
                                        "Authorization": "JWT \(token)"]
            
            Alamofire.request(url,
                              method: parameters != nil ? .patch : .get,
                              parameters: parameters,
                              encoding: JSONEncoding.default,
                              headers: headers)
                .validate()
                .responseJSON { (response) in
                    guard response.result.isSuccess else {
                        print("Error while trying to access: \(String(describing: response.result.error))")
                        completion(self.user)
                        return
                    }
                    
                    guard let value = response.result.value as? [String: Any] else {
                        print("Malformed data received from auth API")
                        completion(self.user)
                        return
                    }
                    
                    self.user = User(json: value)
                    
                    completion(self.user)
            }
        } catch _ {
            completion(self.user)
            return
        }
    }
}

struct User : Codable {
    let id: Int?
    let username: String?
    let age: Int?
    let height: Int?
    let likesJavascript: Bool?
    let magicNumber: Int?
    let magicHash: String?
    
    init(json: [String: Any]) {
        self.id = json[APIKey.Id.rawValue] as? Int
        self.username = json[APIKey.Username.rawValue] as? String
        self.age = json[APIKey.Age.rawValue] as? Int
        self.height = json[APIKey.Height.rawValue] as? Int
        self.likesJavascript = json[APIKey.LikesJS.rawValue] as? Bool
        self.magicNumber = json[APIKey.MagicNumber.rawValue] as? Int
        self.magicHash = json[APIKey.MagicHash.rawValue] as? String
    }
}
