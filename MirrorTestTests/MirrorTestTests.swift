//
//  MirrorTestTests.swift
//  MirrorTestTests
//
//  Created by François Lambert on 23/07/2018.
//  Copyright © 2018 François Lambert. All rights reserved.
//

import XCTest
@testable import MirrorTest

class MirrorTestTests: XCTestCase {
    
    var user: User!
    var viewController: MockAccountViewController!
    var interactor: AccountInteractor!
    
    class MockAccountViewController: AccountViewController {
        
        override func viewDidLoad() {}
        
        override func layout(_ model: AccountViewModel) {
            
            // Test Username display
            if let rowUsername = model.rowViewModel(forIndexPath: IndexPath(row: 0, section: 0)) as? AccountRow {
                XCTAssertTrue(rowUsername.value == "Bob")
                XCTAssertTrue(rowUsername.editable == false)
            }
            // Test Height display
            else if let rowHeight = model.rowViewModel(forIndexPath: IndexPath(row: 0, section: 2)) as? AccountRow {
                XCTAssertTrue(rowHeight.value == "179")
                XCTAssertTrue(rowHeight.editable == true)
            }
            
            // TODO : test Age format
        }
    }
    
    
    override func setUp() {
        super.setUp()
        
        // Scene init
        self.viewController = MockAccountViewController()
        let presenter = AccountPresenter(vc: self.viewController)
        self.interactor = AccountInteractor(presenter: presenter)
        self.viewController.interactor = self.interactor
        
        // Mock user
        let json: [String: Any] = [APIKey.Id.rawValue: 123,
                                   APIKey.Username.rawValue: "Bob",
                                   APIKey.Age.rawValue: 59017851330000, // 29 y.o.
                                   APIKey.Height.rawValue: 179,
                                   APIKey.LikesJS.rawValue: true,
                                   APIKey.MagicNumber.rawValue: 2018,
                                   APIKey.MagicHash.rawValue: "LKS96J5DEX28QT13ZGFG"]
        
        user = User(json: json)   
    }
    
    func testViewModel() {
        interactor.display(user)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        
        user = nil
        viewController = nil
        interactor = nil
    }
    
}
